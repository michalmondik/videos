Rails.application.routes.draw do

  resources :subscribers
  resources :comments
  resources :videos, only: [:show]
  resources :video_categories, only: [:index, :show]

  devise_for :administrators
  devise_for :users

  devise_scope :user do
    get "sign-in", to: "devise/sessions#new"
    get "sign-up", to: "devise/registrations#new"
  end

  namespace :admin do
    resources :videos
    resources :video_categories
    root to: 'videos#index'
  end

  root to: 'root#index'
end
