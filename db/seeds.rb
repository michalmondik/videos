# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

client_email = 'info@videos.cz'


%w(
  m.mondik@gmail.com
  ondrej.pop@gmail.com
  guest@blueberryapps.com
).each do |email|
  Administrator.where(email: email).first_or_create(
    email:        email,
    password:     'heslo123'
  )
end


if Rails.env == 'production'
  User.where(email: client_email)
    .first_or_create email: client_email, password: 'nvc|09-Uáař4'
else
  %w(
    m.mondik@gmail.com
    ondrej.pop@gmail.com
    guest@blueberryapps.com
  ).each do |email|
    User.where(email: email).first_or_create(
      email:        email,
      password:     'heslo123'
    )
  end
end

VideoCategory.create [
  { name: 'Traveling' },
  { name: 'GoPro' },
  { name: 'Funny' },
  { name: 'Cats' },
  { name: 'Car crashes' },
  { name: 'Other' }
]

for i in 0..5
  Video.create [
    { title: 'AMAZING ROAD TRIP #2 - MIAMI, NYC, PARIS, LONDON', link: 'https://www.youtube.com/watch?v=nwyZSGK2KHg', video_category_id: ((i % 6) + 1 )},
    { title: 'AMAZING ROAD TRIP #1 - LOS ANGELES,LAS VEGAS, SAN FRANCISCO,NEW YORK, SAN DIEGO', link: 'https://www.youtube.com/watch?v=amBSypE-r0g', video_category_id: ((i % 6) + 1 ) },
    { title: 'AMAZING ROAD TRIP #3 LA, LV, SD, SF, PALM SPRINGS', link: 'https://www.youtube.com/watch?v=680HNm21w7c', video_category_id: ((i % 6) + 1 ) },
    { title: 'GoPro HD: Shark Riders - Dive Housing', link: 'https://vimeo.com/42714082', video_category_id: ((i % 6) + 1 ) },
    { title: 'Near Space Balloon Flight, shot with HD HERO cameras from GoPro', link: 'https://vimeo.com/12488149', video_category_id: ((i % 6) + 1 ) },
    { title: 'GoPro-GoPug', link: 'https://vimeo.com/29922550', video_category_id: ((i % 6) + 1 ) }
  ]
end

for i in 0..(Video.count - 1)
  #big thumbnails
  file = File.join(Rails.root,'/app/assets/images/thumb/', (i%6).to_s + '_big.jpg')

  #small thumbnails
  # file = File.join(Rails.root,'/app/assets/images/thumb/', (i%6).to_s + '.jpg')

  io = File.new(file)

  video = Video.find(i+1)
  video.set_thumbnail_image_seed(io)
  video.save
end

Comment.create [
  { body: 'muj comment'},
  { body: 'muj druhy comment'},
  { body: 'muj treti comment'}
]

Comment.all.each do |comment|
  comment.video_id = 1
  comment.save
end
