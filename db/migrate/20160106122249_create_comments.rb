class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_od
      t.text :body
      t.integer :video_id

      t.timestamps null: false
    end
  end
end
