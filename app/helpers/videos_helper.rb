module VideosHelper
  def embed(youtube_url)
    youtube_id = youtube_url.split("=").last
    content_tag(:iframe, nil, src: "//www.youtube.com/embed/#{youtube_id}")
  end

  def categories_collection
    VideoCategory.order(:name)
  end

  def main_video
    Video.random_video
  end
end
