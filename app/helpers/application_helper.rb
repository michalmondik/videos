module ApplicationHelper
  VideoInfo.provider_api_keys = { youtube: 'AIzaSyBB2fLMUTiczIwH5mYmOnSgVFICWsLVbk4', vimeo: '6ec85e9f59eab6f3538018f8b1d0ff9a' }

  def t(_, options = {})
    translation = super
    return translation unless translation.is_a?(Array)

    translations = translation.map do |str|
      content_tag(options[:each_wrapper] || :span, str.html_safe)
    end

    translation = translations.join.html_safe

    if options[:wrapper]
      content_tag(options[:wrapper], translation)
    else
      translation
    end
  end

  def meta_robots_tag
    content = Rails.env.production? ? 'index,follow' : 'noindex,nofollow'
    content_tag(:meta, nil, name: 'robots', content: content)
  end

  def bootstrap_alert_class(kind)
    case kind.to_s
    when 'error' then 'alert-danger'
    else "alert-#{kind}"
    end
  end

  def wide_screen(wide = nil)
    @wide_screen ||= wide
  end
end
