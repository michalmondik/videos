class VideoCategory < ActiveRecord::Base
  validates :name, uniqueness: true

  has_many :videos

  def title
    self.name
  end

  def random_video
    offset = rand(self.videos.count)
    rand_record = self.videos.offset(offset).first
    rand_record
  end

  def random_video_link
    random_video.embed_link
  end

  #returns all category videos except one given video
  def other_videos video
    other_videos = self.videos.all
    other_videos.to_a.delete(video)  #removes video from listed videos
    other_videos
  end

  #returns 6 random videos, given video excluded
  def six_random_videos video
      videos = self.videos.all
      videos.to_a.delete(video)
      videos.sample(6)
  end

  def three_random_videos
      # videos = self.videos.sample(3)
      # videos.sample(3)
      self.videos.sample(3)
  end
end
