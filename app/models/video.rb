class Video < ActiveRecord::Base
  belongs_to :video_categories
  has_many :comments
  has_attached_file :image
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  VIMEO_LINK_FORMAT = /(?:vimeo(?:-nocookie)?\.com\/)(([0-9]{8})|([0-9]{9}))/i
  
  VIMEO_LINK_FORMAT2 = /https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/i
  
  YT_LINK_FORMAT = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i

  YT_VIMEO_LINK_FORMAT = /((?:vimeo(?:-nocookie)?\.com\/)((\d{8})|(\d{9})))|((?:youtube(?:-nocookie)?\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11}))/i

  validates :link, presence: true, format: YT_VIMEO_LINK_FORMAT
  validates :video_category_id, presence: true

  def set_thumbnail_image url
    self.image = URI.parse(url)
  end

  def set_thumbnail_image_seed file
    self.image = file
  end

  def category
    VideoCategory.find(self.video_category_id)
  end

  def youtube_id
    self.link.split("=").last
  end
  
  def thumbnail
    if Rails.env == 'production'
      VideoInfo.new(self.link).thumbnail_large
    else
      self.image
    end
  end

  def embed_link
    VideoInfo.new(self.link).embed_url
  end

  def Video.random_video
    offset = rand(Video.count)
    Video.offset(offset).first
  end
end
