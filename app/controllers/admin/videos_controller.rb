class Admin::VideosController < AdminController
  before_action :authenticate_administrator!, only: [:index, :new, :edit, :create, :update, :destroy]
  before_action :set_video, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  # GET /videos
  def index
    @videos = Video.all.page params[:page]
  end

  # GET /videos/1
  def show
    @comments = @video.comments.all
    @comment = @video.comments.build
    #@comment = Comment.new
  end

  # GET /videos/new
  def new
    @video = Video.new
  end

  # GET /videos/1/edit
  def edit
  end

  # POST /videos
  def create
    @video = Video.new(video_params)

    if @video.save
      video_info = VideoInfo.new(@video.link)
      @video.set_thumbnail_image(video_info.thumbnail_large)

      if @video.title == "" then
        @video.title = video_info.title
      end
      @video.save
      redirect_to @video, notice: 'Video was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /videos/1
  def update
    if @video.update(video_params)
      redirect_to @video, notice: 'Video was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /videos/1
  def destroy
    @video.destroy
    redirect_to videos_url, notice: 'Video was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = Video.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def video_params
      params.require(:video).permit(:title, :link, :video_category_id)
    end
end
