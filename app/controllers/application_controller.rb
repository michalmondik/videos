class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :new_subcriber

  def new_subcriber
    @subscriber = Subscriber.new
  end
end
