class VideoCategoriesController < ApplicationController
  before_action :authenticate_administrator!, only: [:new, :edit, :create, :update, :destroy]
  before_action :set_video_category, only: [:show, :edit, :update, :destroy]

  # GET /video_categories
  def index
    @video_categories = VideoCategory.all
  end

  # GET /video_categories/1
  def show
    @videos_in_category = @video_category.videos
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video_category
      @video_category = VideoCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def video_category_params
      params.require(:video_category).permit(:name)
    end
end
