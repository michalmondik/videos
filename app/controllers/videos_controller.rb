class VideosController < ApplicationController
  before_action :set_video, only: [:show]


  # GET /videos/1
  def show
    @comments = @video.comments.all
    @comment = @video.comments.build
    #@comment = Comment.new
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = Video.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def video_params
      params.require(:video).permit(:title, :link, :video_category_id)
    end
end
