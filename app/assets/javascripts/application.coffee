#= require jquery
#= require jquery-ujs
#= require bootstrap-sass/bootstrap
#= require placeholders
#= require jquery.scrollTo
#= require share-button

#= require_directory ./common
#= require_directory ./frontend
